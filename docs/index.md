# URBANITE Forum 

URBANITE Forum is based on [Decidim](https://decidim.org/), a participatory democracy platform created by Barcellona City Hall.

This guide is divided into two separate sections:

-   [Assembly Administrator Guide](admin/index.md)
-   [User Guide](user/index.md)

It's important to underline that this guide gives an overview about the functionalities provided by Decidim platform within URBANITE Project. Please, refer to the official [Decidim documentation](https://docs.decidim.org/en/) to have a complete overview of the platform. 

## Installation Overview

URBANITE’s installation is based on the concept of [Assemblies](https://docs.decidim.org/en/admin/assemblies/). An **Assembly**, within Decidim, is used to configure bodies such as Municipal Councils or City Assemblies.
To access the platform a user should be registered and should provide valid credentials during the login procedure.

The following assemblies are provided:

- **URBANITE General**
- **Amsterdam**
- **Bilbao**
- **Helsinki**
- **Messina**

<span style="display:block;text-align:center">![Assemblies](./images/assemblies.png)</span>

URBANITE General is a **public** assembly, this means that all the registered user in the platform can access, see its contents, and interact with it, creating comments, debates or proposals. Amsterdam, Bilbao, Helsinki and Messina are **private** assemblies, this means that the user needs to be invited by the assembly manager to view it and access its contents.

## Registration

To access the platform a user should be registered and should provide valid credentials during the login procedure.
New registered users:

 - By default, will be able to use URBANITE General assembly functionalities
 - Should wait for the invitation of a City Assembly’s Administrator to participate in the city assembly

<span style="display:block;text-align:center;">![Registration](./images/registration.png)</span>