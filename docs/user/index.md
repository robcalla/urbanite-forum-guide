# Introduction

This section of the guide covers the available end-user functionalities and the interactions that can be performed with the platform.

To access the platform a user should provide valid credentials through the login procedure.

<span style="display:block;text-align:center">![Login](../images/login.png)</span>

Once the user enters the platform he/she will see the homepage composed by the following sections:

- The general navbar and a banner about the platform, by clicking on the *Participate* button the user will be redirected to the profile page.

<span style="display:block;text-align:center">![Homepage](../images/homepage1.png)</span>

- The URBANITE General direct access links: this links helps the user accessing to the **public** assembly content.

<span style="display:block;text-align:center">![URBANITE General](../images/homepage2.png)</span>

- The list of the most recent activities.

<span style="display:block;text-align:center">![Recent Activities](../images/homepage3.png)</span>

- A summary about the number of participants and public assemblies in the platform.

<span style="display:block;text-align:center">![Participants](../images/homepage4.png)</span>