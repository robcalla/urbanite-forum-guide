## User Menu

Through the links at the top right corner of the GUI the user can change the locale of the platform, see the notifications and messages and access to his/her personal profile and information.

<span style="display:block;text-align:center">![User Menu](../images/usermenu1.png)</span>

To modify his/her personal profile, the user should click on the *My account* link.

<span style="display:block;text-align:center">![User Account](../images/usermenu2.png)</span>

Please check Decidim's official documentation to gain insights about each profile page.

## Assemblies

By clicking on the **Assemblies** link in the navbar, the user accesses to the assemblies he/she belongs to. The user will certainly be able to interact with the URBANITE General assembly, since it is a **public** assembly, and all of the **private** assemblies the user was invited. The user will be able to follow the specific assembly, by clicking the *Follow* button, or to access its details, by clicking on the *Take Part* button or the assembly's title itself.

<span style="display:block;text-align:center">![User Assemblies](../images/user_assemblies.png)</span>

## Assembly Details

Once the user accesses the assembly's detail, its general information will be displayed along with the related documents and folders attached. Through the dedicated button the user will be able to download the files.

<span style="display:block;text-align:center">![Assembly details](../images/user_assemblies2.png)</span>

The navbar contains the enabled and configured components.

<span style="display:block;text-align:center">![User Navbar](../images/user_assemblies3.png)</span>

## Blog

To access the blog posts, the user should click the *Blog* link in the assembly's navbar. The user will not be able to add new posts, but he/she will access the comments reported in the bottom part of the page.

<span style="display:block;text-align:center">![Blog](../images/user_blog.png)</span>

## Debates

To access the debates, the user should click the *Debates* link in the assembly's navbar. The user will see the existing debates and will be able to see their details by clicking on the *Participate* button. To contribute to a debate, the user should comment it through the dedicated comment section. 

<span style="display:block;text-align:center">![Debates](../images/user_debates.png)</span>

The following picture depicts a debate detail page.

<span style="display:block;text-align:center">![Debate Detail](../images/user_debates1.png)</span>

The user is allowed to create new debates by clicking on the *New Debate* button. The user is prompted with a form he/she needs to fill, providing the title and the content of the new debate.

<span style="display:block;text-align:center">![New Debate](../images/user_debates2.png)</span>

## Proposals

To access the proposals, the user should click the *Proposals* link in the assembly's navbar. The user will see the existing proposals with their status (if any) and will be able to see their details by clicking on their title button, to support a proposal by clicking on the *Support* button. The user can also comment a proposal through the dedicated comment section.

<span style="display:block;text-align:center">![Proposals](../images/user_proposals.png)</span>

The following picture depicts a proposal detail page. Where the user can see and download the attached files, if any.

<span style="display:block;text-align:center">![Proposal Detail](../images/user_proposals1.png)</span>

Through the *New Proposal* button, the user will be able to create new proposals providing the required information through the form.

<span style="display:block;text-align:center">![New Proposal](../images/user_proposals2.png)</span>

## Comments

In the bottom part of the Blog, Debates and Proposals pages, the user will see the comments and will be able to add new comment, to reply to existing comments or to report a problem about a comment, if any.
To add new comments, the user should fill the form and submit it, providing also an opinion about the topic (Neutral, In-favour or Against). Moreover, the user is able to reply to existing comments through the *Reply* link.

<span style="display:block;text-align:center">![Comments](../images/user_blog_comments.png)</span>

## Report

The user, both in a single comment or proposal, will find the following icon that is used to report problems that will be further moderated by the administrator.

<span style="display:block;text-align:center">![Report](../images/report_problem.png)</span>

By clicking this icon, the following form is provided where the user can specify his/her issue.

<span style="display:block;text-align:center">![Report Form](../images/report_problem1.png)</span>
