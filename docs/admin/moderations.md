# Moderations

This section is used to [Moderate](https://docs.decidim.org/en/admin/process_moderations/) an assembly.

All of the reports provided by the users will be listed into this section. The administrator is in charge of evaluating the report and choose between hiding the content or deleting the complaint.

<span style="display:block;text-align:center">![Moderations](../images/moderation.png)</span>

By default, the administrator will see the *NOT HIDDEN* reports and the actions that he/she can perform will be to **Hide** the content or to **Unreport** it. 
Once the content is hidden the only available action will be to **Unhide** the content. 

<span style="display:block;text-align:center">![Hidden Moderations](../images/moderation1.png)</span>