# Attachments

This section is used to manage Assebly's [Attachments](https://docs.decidim.org/en/admin/process_attachments/). The administrator of the assembly can upload file or can create folders to group files.

<span style="display:block;text-align:center">![Attachments](../images/attachment1.png)</span>

## Folders

To create or manage folders, the administrator should click on the *Folders* link. To create a new folder, the administrator should click on the *New Folder* button and filling the provided form.

<span style="display:block;text-align:center">![Folders](../images/attachment_folder1.png)</span>

<span style="display:block;text-align:center">![Create Folder](../images/attachment_folder2.png)</span>

## Files

To upload or manage files, the administrator should click on the *Files* link. To create a new attachment, the administrator should click on the *New Attachment* button and filling the provided form, selecting, if needed, an existing folder.

<span style="display:block;text-align:center">![Attachments](../images/attachment_file1.png)</span>

<span style="display:block;text-align:center">![Create Attachment](../images/attachment_file2.png)</span>
