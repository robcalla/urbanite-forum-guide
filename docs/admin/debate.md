# Debate Component

This section describes the management of the [Debates](https://docs.decidim.org/en/admin/component_debates/) component.

## Configuration

To configure the Blog component, the administrator should add the component to the assembly or modify it, by clicking on the *Configure* icon in the [Components](components.md) page.

The administrator through the provided form will be able to configure of the component (e.g. changing its name). The configurations are divided into the *Global settings* and the *Default step settings*. It is important to underline that end users will be able to comment the posts if **Comments enabled** is checked. Moreover, end users will be able to create debates if the **Debate creation by participants enabled** is checked. 

<span style="display:block;text-align:center">![Debates Configuration](../images/debate0.png)</span>

## Debates management 

By clicking on the *Manage* icon in the [Components](components.md) page or the Debates link on the dashboard menu the administrator accesses to the Debates management section. 

The administrator through this section of the platform will be able to create a new debate and to manage the existing ones. To create a new debate the administrator should click on the *New Debate* button and fill the provided form.

<span style="display:block;text-align:center">![Debate](../images/debate1.png)</span>

<span style="display:block;text-align:center">![New Debate](../images/debate2.png)</span>

The allowed actions over an existing debate are *delete* and *edit*. It is important to underline that the administrator cannot modify or delete debates created by end users.





