# Blog Component

This section describes the management of the **Blog** component.

## Configuration

To configure the Blog component, the administrator should add the component to the assembly or modify it, by clicking on the *Configure* icon in the [Components](components.md) page.

The administrator through the provided form will be able to configure of the component (e.g. changing its name). The configurations are divided into the *Global settings* and the *Default step settings*. It is important to underline that end users will be able to comment the posts if **Comments enabled** is checked. 

<span style="display:block;text-align:center">![Blog Configuration](../images/blog0.png)</span>

## Post Management

By clicking on the *Manage* icon in the [Components](components.md) page or the Blog link on the dashboard menu the administrator accesses to the Post management section. 

The administrator through this section of the platform will be able to create a new post and to manage the existing ones. In order to create new posts, the administrator should click on the *New Post* button and providing the post title and the content.

<span style="display:block;text-align:center">![Blog](../images/blog1.png)</span>

<span style="display:block;text-align:center">![Blog Form](../images/blog2.png)</span>

The following actions can be performed on existing posts:

<span style="display:block;text-align:center">![Blog Actions](../images/blog3.png)</span>

- **Edit**: with this action the administrator can modify the title and content of the post.
- **Folders**: with this action the administrator add one or more folders to the post. Moreover, he/she will be able to see, modify or delete the existing ones.
- **Attachments**: with this action the administrator add one or more attachments to the post. Moreover, he/she will be able to add the attachment to an existing folder.
- **Delete**: with this action the administrator will delete the post and all of the related folders and attachments, if any.
