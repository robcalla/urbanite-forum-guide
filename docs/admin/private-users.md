# Private Users

This section is used to manage private users and to invite users to a **private assembly**. The private assembly will be hidden for any user not listed into the private users.

The administrator can invite a single user or can upload a csv file containing the email and the name of the new users by clicking, respectively, the *New Participatory Space Private Participant* or the *Import Via CSV*.

<span style="display:block;text-align:center">![Private Users](../images/private_user1.png)</span>

<span style="display:block;text-align:center">![Invite User](../images/private_user2.png)</span>

<span style="display:block;text-align:center">![Invite Users CSV](../images/private_user3.png)</span>