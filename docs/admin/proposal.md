# Proposal Component

This section describes the management of the [Proposals](https://docs.decidim.org/en/admin/component_proposals/) component.

## Configuration

To configure the Proposals component, the administrator should add the component to the assembly or modify it, by clicking on the *Configure* icon in the [Components](components.md) page.

The administrator through the provided form will be able to configure of the component (e.g. changing its name). The several configurations are divided into the *Global settings* and the *Default step settings*.

<span style="display:block;text-align:center">![Proposal Configuration](../images/proposals1.png)</span>

Among the several configurations available, to allow users to create proposals the **Proposal creation enabled** must be checked, under the *Default step settings*. Moreover, the administrator can give the chance to users to *amend* the proposal by checking the **Amendments enabled** configuration property under the *Global settings*.

<span style="display:block;text-align:center">![Proposal Amend](../images/proposals2.png)</span>

If the property is enabled, under the *Default step settings* the following properties will be showed.

<span style="display:block;text-align:center">![Proposal Amend Config](../images/proposals3.png)</span>

Please refer to Decidim's official documentation for the description of each field.

## Proposals Management

By clicking on the *Manage* icon in the [Components](components.md) page or the Proposals link on the dashboard menu the administrator accesses to the Proposals management section. 

The administrator through this section of the platform will be able to create a new proposal and to manage the existing ones. The following picture depicts the *Proposal* section of the platform. 

<span style="display:block;text-align:center">![Proposals](../images/proposals4.png)</span>

The platform provides several functionalities through this page:

- Create new Proposal
- Import proposals from another component
- Export proposals
- Filtering the existing proposals
- Actions on the Proposals

### Create new Proposal

To create a new proposal the administrator should click on the *New Proposal* button and fill the provided form inserting the name of the proposal and its content. The default status of a proposal is **Not answered** and it means that the proposal should be further evaluated.

<span style="display:block;text-align:center">![New Proposal](../images/proposals7.png)</span>

### Import Proposals from another component

To import proposals from another component it is **necessary** that at least **two** Proposal components are defined within the assembly. By clicking on the dedicated button, the following form is provided.
The administrator will be able to select the origin component and to filter the proposals to be imported by their status.

<span style="display:block;text-align:center">![Import Proposal](../images/proposals5.png)</span>

### Export Proposals

By clicking the *Export* button the administrator can choose to export proposals or comments in different formats (CSV, JSON or EXCEL). The results will be sent to the user email once it is ready.

<span style="display:block;text-align:center">![Export Proposal](../images/proposals6.png)</span>

### Filtering Proposals

The administrator can filter the several proposals clicking the *Filter* button or providing the id or the title of the proposals.

<span style="display:block;text-align:center">![Filter Proposal](../images/proposals_filters.png)</span>

### Actions on the Proposals

The following actions can be performed on the existing proposals:

- Edit Proposal: this action is enabled only if the author of the proposal is the administrator himself/herself, and enable to modify the proposal
- Answer Proposal: this action allows to evaluate the proposal.
- Permission: this action allows to manage [permissions](https://docs.decidim.org/en/develop/permissions/), if defined.
- Preview: this action allows to preview the proposal.

<span style="display:block;text-align:center">![Actions](../images/proposals_actions.png)</span>

Moreover, by selecting one proposal, through the checkbox on the left, another *Actions* button will appear in the top menu.

<span style="display:block;text-align:center">![Actions Top Menu](../images/proposals_evaluation.png)</span>

By clicking on such button, the administrator will be able to perform the following additional actions.

<span style="display:block;text-align:center">![Actions Menu](../images/proposals_actions_menu.png)</span>

Among such actions, it is important to underline the *Assign to Valuator* one. Through this action, the administrator will assign the proposal to a **Valuator** if exists in the assembly.
To assign the proposal, the administrator should click on the button, select a valuator from the provided list and, finally, click on the *Assign* button.

<span style="display:block;text-align:center">![Assign](../images/proposals_evaluation1.png)</span>

## Proposals Evaluation

To evaluate the proposal, the administrator should click on the *Answer Proposal* button. The user will be redirected to the evaluation page composed by three sections.

- Proposals details: a summary about the proposal.
- Private Notes: where the administrator can submit notes about the proposal.
- Answer for Proposal: this is the specific section where the administrator can effectively evaluate the proposal. He/she can select the output status (Accepted, Rejected, Evaluating) providing also the rationale about the chosen status.

<span style="display:block;text-align:center">![Details](../images/proposals_evaluation2.png)</span>

<span style="display:block;text-align:center">![Notes](../images/proposals_evaluation3.png)</span>

<span style="display:block;text-align:center">![Evaluation](../images/proposals_evaluation4.png)</span>



