# General Information

This section of the Assembly administrator dashboard is used to manage the **General Information** of an assembly, for example its title and description. 

<span style="display:block;text-align:center">![Info](../images/info.png)</span>

The administrator will also be able to set a **home** and a **banner** image that will be used, respectively, in the presentation card of the assembly and in the assembly page.

<span style="display:block;text-align:center">![Images](../images/info_images.png)</span>

Moreover, the administrator can set the visibility of the assembly.

<span style="display:block;text-align:center">![Visibility](../images/info_visibility.png)</span>

For the description of the other fields, please refer to [Decidim's official documentation](https://docs.decidim.org/en/admin/processess/).