# Introduction

The Assembly Administrator is in charge of managing the assembly (one or more). He/she can manage every aspect and functionality of the assembly.

To access the administrative functionalities, the user should login in the platform providing valid administrative credentials, and click on the **Admin dashboard** link listed under the user menu, as depicted in the following pictures.

<span style="display:block;text-align:center">![Menu](../images/menuadmin1_.png)</span>

<span style="display:block;text-align:center">![Admin Dashboard Link](../images/menuadmin2.png)</span>

## Dashboard Overview

The **Admin Dashboard** is used by the administrators to manage the assemblies. He/she will be able to see and filter all of the existing assemblies, but he/she is allowed to modify only the ones assigned to him/her.

<span style="display:block;text-align:center">![Admin Dashboard](../images/dashboard1.png)</span>

Through the **Filter** the administrator will be able to filter the assemblies considering their scope (private or public) or their status (published or unpublished). Moreover, the administrator will be able to filter the assemblies by their title.

<span style="display:block;text-align:center">![Filters](../images/filters.png)</span>

As already depicted, the *messina_admin* user is in charge of managing *Messina*'s assembly. Through the buttons provided by the dashboard he/she will be able to configure the assembly or to see its preview.

<span style="display:block;text-align:center">![Messina](../images/messina.png)</span>

## Assembly Menu

By clicking on the **Configure** the user will be able to see and manage the assembly details.

<span style="display:block;text-align:center">![Assembly Menu](../images/admin_menu.png)</span>

From the assembly menu, the administrator will be able to manage:
 
 - [**Info**](info.md)
 - [**Components**](components.md)
 - [**Categories**](categories.md)
 - [**Attachments**](attachments.md)
 - [**Members**](members.md)
 - [**Assembly admins**](assembly-admins.md)
 - [**Private Users**](private-users.md)
 - [**Moderations**](moderations.md)

<span style="display:block;text-align:center">![Assembly Menu Detail](../images/admin_menu1.png)</span>

## Multilanguage Support

URBANITE's installation of Decidim provides **multilanguage** support, giving administrators the chance of providing different translations of the text fields. The default language is **English** thus, the administrator should at least provide English version of any text field. 

<span style="display:block;text-align:center">![Multilanguage](../images/multilanguage.png)</span>

To provide a different translation of a text field, the administrator should select one of the available languages from the list on top of each input field and write the proper translation.