# Members

This section, if used, will communicate to participants who are the *official* members of an assembly with their specific position in the assembly itself. This list is not automatically filled by the system; thus the administrator is in charge of adding the members. To include a new member the administrator should click on the *New Member* and fill the provided form. 

It's important to underline that the listed **Positions** are not used by the platform as roles.

<span style="display:block;text-align:center">![Members](../images/member1.png)</span>

<span style="display:block;text-align:center">![Create Member](../images/member2.png)</span>

