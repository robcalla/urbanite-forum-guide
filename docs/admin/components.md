# Components

This section of the dashboard is used to manage assembly's components. Components are the main building blocks for every participatory process, like the assembly, within Decidim.

Decidim provides the following components, please check Decidim official documentation to read the complete description of each component:

| Component | Description | 
| --------- | ----------- |
|[Accountability](https://docs.decidim.org/en/admin/component_accountability/)|This component provides the level of execution of the results of the assembly.|
|[Blog]()|This component is used by the administrator to provide useful insights about the assembly.|
|[Budgets](https://docs.decidim.org/en/admin/component_budgets/)|This component provides the means to create participatory budgeting processes.|
|[Debates](https://docs.decidim.org/en/admin/component_debates/)|This component act as the *municipality forum* allowing users to open questions or discussions within the assembly.|
|[Meetings](https://docs.decidim.org/en/admin/component_meetings/)|This component is used to schedule online or in-person meetings.|
|[Page](https://docs.decidim.org/en/admin/component_pages/)|This components allows to add informative pages within the assembly.|
|[Proposals](https://docs.decidim.org/en/admin/component_proposals/)|This component provide the means to create ideas and challenges, the so-called **proposals**, within the assembly. Proposals will be further evaluated by the administrators.|
|[Sortitions](https://docs.decidim.org/en/admin/component_sortitions/)|This component allows to randomly select a group of users from the proposals to create a pool of candidates to build, for instance, a committee|
|[Surveys](https://docs.decidim.org/en/admin/component_surveys/)|This component allows to create surveys.|

## Manage Components

To add new component for an assembly the administrator should access the specific section by clicking the *Components* menu item. The configured components will be listed to the administrator.

<span style="display:block;text-align:center">![Components](../images/components1.png)</span>

To create a new component the administrator should click the *Add Component* button, select the desired component and fill the necessary information, please check Decidim documentation to see the details of the components.

By clicking on each *Component name*, the administrator will access the preview of the component. Moreover, the following actions can be performed:

<span style="display:block;text-align:center">![Actions](../images/components2.png)</span>

- **Manage**: with this action, the administrator will access the component elements (e.g. the existing proposals for the *Proposals* component)
- **Publish / Unpublish**: with this action, the administrator will, respectively, show or hide the component, and its elements, to the final user. 
- **Configure**: with this action, the administrator will edit the component's general configurations.
- **Permissions**: with this action the administrator will manage the [permissions](https://docs.decidim.org/en/develop/permissions/), if some permission exists.
- **Delete**: with this action, the component will be deleted from the assembly. 

Moreover, the administrator menu will report the configured components giving the number their existing elements. By clicking on such menu element, the administrator will access the component elements.

<span style="display:block;text-align:center">![Component Menu](../images/components3.png)</span>

## Configured Component

Within URBANITE Forum platform, the configured components, for each assembly, are:

- [Blog](blog.md)
- [Debates](debate.md)
- [Proposals](proposal.md)


