# Categories

[Categories](https://docs.decidim.org/en/admin/process_categories/) can be used to classify the assembly content, for instance a debate. Moreover, each category and sub-category can be used to filter the assembly content.

<span style="display:block;text-align:center">![Categories](../images/category1.png)</span>

To create a new category, click on the *New Category* button and fill the following form. To create a sub-category, select an existing category in the *Parent* field.

<span style="display:block;text-align:center">![Create Category](../images/category2.png)</span>
