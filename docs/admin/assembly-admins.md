# Assembly Admins

This section of the dashboard is used to assign and manage **additional** administrators for the assembly. The additional administrators are optional since a single administrator can perform any operation within an assembly.
 
To create a new administrator or to assign a role to an existing user, the administrator should click on the *New Assembly Admin* button

<span style="display:block;text-align:center">![Admins](../images/admins_1.png)</span>

The administrator needs to provide a name, an email and the role to be assigned. It is important to underline that if the email belongs to an existing user, the name will be overwritten by the platform with the one already stored.

<span style="display:block;text-align:center">![Admins](../images/admins_2.png)</span>

## Roles 

The following table describes each role. 

| Role Name      | Description         |
| -------------  | --------------- |
| Administrator  | The Administrator of the assembly. He/she is allowed to perform any operation.|
| Collaborator   | A Collaborator manages the components of an assembly. For instance, he/she is in charge of assign the proposals to the specific valuator or he/she can evaluate the proposals by himself.|
| Moderator   | A Moderator moderates the assembly. If any post/comment is reported, he/she will be able to see and manage the report through the Assembly's Administrator dashboard.|
| Valuator  | A Valuator is in charge of evaluating the proposals assigned to him/her. He/she can only see the proposal section in the administrator dashboard. |
 

